Name:		example
Version:	1
Release:	1%{?dist}
Summary:	This is a simple example to test copr

License:	GPLv2+
URL:		https://gitlab.com/copr-project/webhook-testing-fedora-copr
Source0:	%{name}-%{version}.tar.gz

BuildRequires:	gcc


%description
Simple example package.


%prep
%setup -q


%build
make %{?_smp_mflags}


%install
install -d %{buildroot}%{_sbindir}
cp -a main %{buildroot}%{_sbindir}/main


%files
%doc
%{_sbindir}/main

%changelog
* Sat Jun 15 2019 Pavel Raiskup <praiskup@redhat.com> - 1-1
- no changelog in git
