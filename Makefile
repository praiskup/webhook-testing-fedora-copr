CC = gcc

all: program

program: main.o
	$(CC) -o $@ $^

main.o: main.c

clean:
	rm *.o program
